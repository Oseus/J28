### lien heroku: https://arcane-harbor-99289.herokuapp.com/

## Introduction

Nous avons divisé le projet du jour plusieurs étapes :
1. Landing page : nous avons utilisé un template pour être plus efficaces donc nous l'avons intégré a notre app Rails
2. Call To action : Pour le call To action : il y en a  2 : un pour s'inscrire à la newsletter (inexistante...) et l'autre qui est plus efficace et direct qui renvoie à la page de l'événement THP Facebook du 29 mai à Lyon...
3. Acquisition : Nous avons choisi le bot twitter pour la stratégie acquisition mais il en court de développement...
4. Analyse des metrics : Nous avons utilisé google Analytics et coller quelques lignes de code
5. Le rendu : il est beau , n'est-ce pas ? 

## Organisation

* Mohammad-ALi et Hugo -->    Deux personnes sur le front : design, code, installation des outils (metrics, Mailchimp, etc)
* David et Odyssey -->   Deux personnes sur l'acquisition (code de différents outils)
* Tout le monde -->   Tout le monde rédige une partie de la synthèse

## How it works ?

Pour faire tourner tous les fichiers, il est nécessaire d'installer plusieurs gems.
Pour cela, rien de plus simple : une fois le dossier cloné sur l'ordinateur,

```$ git clone https://github.com/Hug-O/J28.git```

il suffit d'accéder au dossier et de faire :

```$ rails s```

Et voilà, vous pouvez voir notre site !


## Petit Bilan :

- [x] Landing page
- [x] Call to action
- [x] Analyse des metrics
- [x] Le rendu
- [ ] Acquisition/twitter

:pray: Indulgence required, we did our best :smile:

## Améliorations possibles :
Si nous aurions eu plus de temps, nous aurions pu améliorer grandement la stratégie acquisition et la déployer via d'autres réseaux comme Instagram, Facebook, avoir plus de cibles sur Twitter, contacter par mail les organismes d'orientation/lycées....


## Construit avec  :

* Atom
* et la super team #elonmuskrew

## La Team :heart: **_ Hugo & David & Odyssey & Mohammad-Ali _** :

## Contributors with peer-learning : :love_letter:

* Mohammad-Ali: https://github.com/mohammadali-bacha
* Odssey: https://github.com/Odssey
* oseus: https://github.com/oseus
* Hug-O: https://github.com/Hug-O

## Slack des contributeurs :

* Mohammad-Ali : @Mohammad-Ali
* Odyssée Levine : @Odyssée Levine
* David Coat : @Coat David
* Hugo : @Hugo
